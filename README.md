### RUN MICROSERVICE
You just need to go to the main folder and execute docker-compose up this should build and run all 4 microservices in your docker 

### TECH USED
IMPORTANT : JAVA 11 REQUIRED.
I used an eureka- feing aproach to connect Spring-boot microservices and an h2 internal db in the subscription-service.
Once you run the containers you can test it with the postman-collection I provide : Adidas.postman_collection.json
This frameworks and tech are the most used in the industry now so I think its the correct stack of tech.
I used lombok (because it helps a lot) and springboot starter libraries.

### SECURITY
I just used Basic Auth to be simple and no external applications required, user: egf password:password

### SWAGGER
You can find the Swagger conf in the postman collection, both swagger-ui and swagger v2.

### DISCLAIMER
I didn't create the kubernetes config files because I cant test it in my pc.
I'm having troubles to link the containers inside docker, I think its because some configuration in my pc that I haven't be able to find so I have tested all connections in my localhost but its should be fine in any other computer.
