package adidas.egf.subscriptionservice.resource;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class EmailServiceController {

  @PostMapping("/email/{email}")
  public ResponseEntity<String> createSubscription(
          @PathVariable String email) {
    log.info("Sending email, email {}", email);
    return new ResponseEntity<>("Notification sent to " + email, HttpStatus.CREATED);
  }
}
