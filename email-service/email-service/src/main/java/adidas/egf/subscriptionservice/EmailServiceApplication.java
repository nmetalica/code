package adidas.egf.subscriptionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "adidas.egf")
public class EmailServiceApplication {


  public static void main(String[] args) {
    SpringApplication.run(EmailServiceApplication.class, args);
  }
}
