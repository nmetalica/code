package adidas.egf.subscriptionservice.resource.mapper;

import adidas.egf.subscriptionservice.application.domain.model.Gender;
import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.resource.model.GetSubscriptionResponse;
import adidas.egf.subscriptionservice.resource.model.SubscriptionRequest;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-01-28T16:00:06+0100",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.1.1.jar, environment: Java 11 (Oracle Corporation)"
)
public class SubscriptionRequestMapperImpl extends SubscriptionRequestMapper {

    @Override
    public Subscription map(SubscriptionRequest request) {
        if ( request == null ) {
            return null;
        }

        Subscription subscription = new Subscription();

        subscription.setSubscriptionId( request.getSubscriptionId() );
        subscription.setName( request.getName() );
        subscription.setEmail( request.getEmail() );
        if ( request.getGender() != null ) {
            subscription.setGender( Enum.valueOf( Gender.class, request.getGender() ) );
        }
        subscription.setBirthDate( request.getBirthDate() );
        subscription.setFlag( request.isFlag() );
        subscription.setNewsletterId( request.getNewsletterId() );

        return subscription;
    }

    @Override
    public GetSubscriptionResponse map(Subscription product) {
        if ( product == null ) {
            return null;
        }

        GetSubscriptionResponse getSubscriptionResponse = new GetSubscriptionResponse();

        getSubscriptionResponse.setSubscriptionId( product.getSubscriptionId() );
        getSubscriptionResponse.setSubscriptionStatus( product.getSubscriptionStatus() );
        getSubscriptionResponse.setName( product.getName() );
        getSubscriptionResponse.setEmail( product.getEmail() );
        getSubscriptionResponse.setGender( product.getGender() );
        getSubscriptionResponse.setBirthDate( product.getBirthDate() );
        getSubscriptionResponse.setFlag( product.isFlag() );
        getSubscriptionResponse.setNewsletterId( product.getNewsletterId() );

        return getSubscriptionResponse;
    }
}
