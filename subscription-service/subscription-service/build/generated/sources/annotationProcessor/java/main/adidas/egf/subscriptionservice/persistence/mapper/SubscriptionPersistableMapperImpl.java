package adidas.egf.subscriptionservice.persistence.mapper;

import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.persistence.repository.model.SubscriptionPersistable;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-01-28T16:00:06+0100",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.1.1.jar, environment: Java 11 (Oracle Corporation)"
)
public class SubscriptionPersistableMapperImpl extends SubscriptionPersistableMapper {

    @Override
    public SubscriptionPersistable map(Subscription subscription) {
        if ( subscription == null ) {
            return null;
        }

        SubscriptionPersistable subscriptionPersistable = new SubscriptionPersistable();

        subscriptionPersistable.setId( subscription.getSubscriptionId() );
        subscriptionPersistable.setSubscriptionStatus( subscription.getSubscriptionStatus() );
        subscriptionPersistable.setName( subscription.getName() );
        subscriptionPersistable.setEmail( subscription.getEmail() );
        subscriptionPersistable.setGender( subscription.getGender() );
        subscriptionPersistable.setBirthDate( subscription.getBirthDate() );
        subscriptionPersistable.setFlag( subscription.isFlag() );
        subscriptionPersistable.setNewsletterId( subscription.getNewsletterId() );

        return subscriptionPersistable;
    }

    @Override
    public Subscription map(SubscriptionPersistable subscriptionPersistable) {
        if ( subscriptionPersistable == null ) {
            return null;
        }

        Subscription subscription = new Subscription();

        subscription.setSubscriptionId( subscriptionPersistable.getId() );
        subscription.setSubscriptionStatus( subscriptionPersistable.getSubscriptionStatus() );
        subscription.setName( subscriptionPersistable.getName() );
        subscription.setEmail( subscriptionPersistable.getEmail() );
        subscription.setGender( subscriptionPersistable.getGender() );
        subscription.setBirthDate( subscriptionPersistable.getBirthDate() );
        subscription.setFlag( subscriptionPersistable.isFlag() );
        subscription.setNewsletterId( subscriptionPersistable.getNewsletterId() );

        return subscription;
    }
}
