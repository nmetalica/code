package adidas.egf.subscriptionservice.application.domain.model;

public enum SubscriptionStatus {
    ACTIVE,CANCELLED
}
