package adidas.egf.subscriptionservice.resource.model;

import adidas.egf.subscriptionservice.application.domain.model.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@Getter
@ToString
public class GetSubscriptionResponse {
  private Long subscriptionId;
  private String subscriptionStatus;
  private String name;
  private String email;
  private Gender gender;
  private Date birthDate;
  private boolean flag;
  private Long newsletterId;
}
