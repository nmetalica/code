package adidas.egf.subscriptionservice.resource.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@AllArgsConstructor
public class SubscriptionResponse {
  private Long subscriptionId;
}
