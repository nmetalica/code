package adidas.egf.subscriptionservice.resource.mapper;

import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.resource.model.SubscriptionRequest;
import adidas.egf.subscriptionservice.resource.model.GetSubscriptionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class SubscriptionRequestMapper {

  public static final SubscriptionRequestMapper MAPPER = Mappers.getMapper(SubscriptionRequestMapper.class);

  public abstract Subscription map(SubscriptionRequest request);

  public abstract GetSubscriptionResponse map(Subscription product);
}
