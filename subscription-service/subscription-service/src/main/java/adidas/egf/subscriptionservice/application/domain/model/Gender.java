package adidas.egf.subscriptionservice.application.domain.model;

public enum Gender {
    MALE,FEMALE,OTHERS
}
