package adidas.egf.subscriptionservice.service;

import adidas.egf.subscriptionservice.common.config.FeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "email", url = "localhost:8084", path = "/emailService", configuration = FeignClientConfiguration.class)
public interface EmailService {

    @PostMapping(value = "/email/{email}")
    String addSubscription(@PathVariable String email);

}
