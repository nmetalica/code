package adidas.egf.subscriptionservice.application.domain.model;

import adidas.egf.subscriptionservice.application.domain.repository.SubscriptionCatalogRepository;
import adidas.egf.subscriptionservice.application.query.model.QueryParameter;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class SubscriptionCatalog {

  private final SubscriptionCatalogRepository subscriptionCatalogRepository;

  public Long addSubscription(Subscription subscription) {
    subscription.setSubscriptionStatus(SubscriptionStatus.ACTIVE.name());
    return subscriptionCatalogRepository.save(subscription);
  }

  public Long cancelSubscription(Subscription subscription) {
    subscription.setSubscriptionStatus(SubscriptionStatus.CANCELLED.name());
    return subscriptionCatalogRepository.save(subscription);
  }

  public void deleteSubscription(Long subscriptionId) {
    subscriptionCatalogRepository.delete(subscriptionId);
  }

  public List<Subscription> getAll() {
    return subscriptionCatalogRepository.getAll();
  }

  public Subscription get(Long productId) {
    return subscriptionCatalogRepository.get(productId);
  }

}
