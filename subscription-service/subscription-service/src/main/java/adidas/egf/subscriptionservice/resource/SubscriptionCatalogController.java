package adidas.egf.subscriptionservice.resource;

import adidas.egf.subscriptionservice.application.factory.SubscriptionCatalogFactory;
import adidas.egf.subscriptionservice.resource.mapper.SubscriptionRequestMapper;
import adidas.egf.subscriptionservice.resource.model.GetSubscriptionResponse;
import adidas.egf.subscriptionservice.resource.model.SubscriptionRequest;
import adidas.egf.subscriptionservice.resource.model.SubscriptionResponse;
import adidas.egf.subscriptionservice.service.EmailService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class SubscriptionCatalogController {

  @Autowired private SubscriptionCatalogFactory subscriptionCatalogFactory;
  @Autowired private EmailService emailService;

  @PostMapping("/subscription")
  public ResponseEntity<SubscriptionResponse> createSubscription(
      @Valid @RequestBody SubscriptionRequest subscriptionRequest) {

    log.info("Creating subscription, name {}", subscriptionRequest.getName());

    var subscription = SubscriptionRequestMapper.MAPPER.map(subscriptionRequest);
    var subscriptionCatalogCommand = subscriptionCatalogFactory.getSubscriptionCatalogCommand();

    var subscriptionId = subscriptionCatalogCommand.addSubscription(subscription);
    var subscriptionResponse = new SubscriptionResponse(subscriptionId);
    String responseEmail = emailService.addSubscription(subscriptionRequest.getEmail());
    return new ResponseEntity<>(subscriptionResponse, HttpStatus.CREATED);
  }

  @GetMapping("/subscription/{subscriptionId}")
  public ResponseEntity<GetSubscriptionResponse> getSubscriptionById(
      @PathVariable @NonNull Long subscriptionId) {

    log.info("Getting subscription, id {}", subscriptionId);

    var subscriptionCatalogQuery = subscriptionCatalogFactory.getSubscriptionQuery();

    var subscription = subscriptionCatalogQuery.getSubscription(subscriptionId);

    var subscriptionResponse = SubscriptionRequestMapper.MAPPER.map(subscription);

    return new ResponseEntity<>(subscriptionResponse, HttpStatus.OK);
  }

  @GetMapping("/subscriptions")
  public ResponseEntity<List<GetSubscriptionResponse>> listSubscriptions() {

    log.info("Getting all subscriptions");

    var subscriptionCatalogQuery = subscriptionCatalogFactory.getSubscriptionQuery();

    var subscriptions = subscriptionCatalogQuery.getSubscriptions();

    var subscriptionResponse =
            subscriptions.stream().map(SubscriptionRequestMapper.MAPPER::map).collect(Collectors.toList());

    return new ResponseEntity<>(subscriptionResponse, HttpStatus.OK);
  }

  @PutMapping("/subscription/{subscriptionId}")
  public ResponseEntity<Void> cancelSubscription(
          @PathVariable @NonNull Long subscriptionId) {

    log.info("Deleting subscription, id {}", subscriptionId);

    var subscriptionCatalogCommand = subscriptionCatalogFactory.getSubscriptionCatalogCommand();
    var subscriptionCatalogQuery = subscriptionCatalogFactory.getSubscriptionQuery();

    var subscription = subscriptionCatalogQuery.getSubscription(subscriptionId);

    subscriptionCatalogCommand.cancelSubscription(subscription);

    return new ResponseEntity<>(HttpStatus.OK);
  }
}
