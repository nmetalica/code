package adidas.egf.subscriptionservice.application.query.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QueryParameter {
  private String status;
  private String mail;
  private Long newsletterId;
}
