package adidas.egf.subscriptionservice.application.command;

import adidas.egf.subscriptionservice.application.domain.model.Subscription;

public interface SubscriptionCatalogCommand {
  Long addSubscription(Subscription subscription);
  Long cancelSubscription(Subscription subscription);
}
