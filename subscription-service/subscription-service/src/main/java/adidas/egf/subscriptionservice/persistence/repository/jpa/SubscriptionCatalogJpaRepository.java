package adidas.egf.subscriptionservice.persistence.repository.jpa;

import adidas.egf.subscriptionservice.persistence.repository.model.SubscriptionPersistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubscriptionCatalogJpaRepository extends JpaRepository<SubscriptionPersistable, Long> {
    @Query("SELECT u FROM SubscriptionPersistable u WHERE u.subscriptionStatus = ?1")
    List<SubscriptionPersistable> findAll(String status);
}
