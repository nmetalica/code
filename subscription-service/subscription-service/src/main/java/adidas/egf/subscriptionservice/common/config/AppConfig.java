package adidas.egf.subscriptionservice.common.config;

import adidas.egf.subscriptionservice.application.query.SubscriptionCatalogQuery;
import adidas.egf.subscriptionservice.persistence.repository.SubscriptionCatalogRepositoryImpl;
import adidas.egf.subscriptionservice.application.command.SubscriptionCatalogCommand;
import adidas.egf.subscriptionservice.application.command.impl.SubscriptionCatalogCommandImpl;
import adidas.egf.subscriptionservice.application.domain.repository.SubscriptionCatalogRepository;
import adidas.egf.subscriptionservice.application.factory.SubscriptionCatalogFactory;
import adidas.egf.subscriptionservice.application.factory.impl.SubscriptionCatalogFactoryImpl;
import adidas.egf.subscriptionservice.application.query.impl.SubscriptionCatalogQueryImpl;
import adidas.egf.subscriptionservice.persistence.repository.jpa.SubscriptionCatalogJpaRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@Setter
public class AppConfig {

  @Autowired private SubscriptionCatalogJpaRepository subscriptionCatalogJpaRepository;



  @Bean
  public SubscriptionCatalogFactory subscriptionCatalogFactory() {
    return new SubscriptionCatalogFactoryImpl(subscriptionCatalogCommand(), subscriptionCatalogQuery());
  }


  private SubscriptionCatalogQuery subscriptionCatalogQuery() {
    return new SubscriptionCatalogQueryImpl(subscriptionCatalogRepository());
  }


  @Bean
  public SubscriptionCatalogCommand subscriptionCatalogCommand() {
    return new SubscriptionCatalogCommandImpl(subscriptionCatalogRepository());
  }


  private SubscriptionCatalogRepository subscriptionCatalogRepository() {
    return new SubscriptionCatalogRepositoryImpl(subscriptionCatalogJpaRepository);
  }

  @Bean
  public BCryptPasswordEncoder encoder() {
    return new BCryptPasswordEncoder();
  }
}
