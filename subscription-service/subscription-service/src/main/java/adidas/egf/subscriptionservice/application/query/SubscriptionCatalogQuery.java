package adidas.egf.subscriptionservice.application.query;


import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.application.query.model.QueryParameter;

import java.util.List;

public interface SubscriptionCatalogQuery {
  Subscription getSubscription(Long subscriptionId);
  List<Subscription> getSubscriptions();
}
