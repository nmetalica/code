package adidas.egf.subscriptionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class SubscriptionCatalogSvcApplication {

  public static void main(String[] args) {
    SpringApplication.run(SubscriptionCatalogSvcApplication.class, args);
  }
}
