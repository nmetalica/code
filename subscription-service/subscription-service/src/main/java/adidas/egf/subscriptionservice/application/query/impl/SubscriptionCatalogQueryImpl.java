package adidas.egf.subscriptionservice.application.query.impl;

import adidas.egf.subscriptionservice.application.query.SubscriptionCatalogQuery;
import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.application.domain.model.SubscriptionCatalog;
import adidas.egf.subscriptionservice.application.domain.repository.SubscriptionCatalogRepository;
import adidas.egf.subscriptionservice.application.query.model.QueryParameter;

import java.util.List;

public class SubscriptionCatalogQueryImpl implements SubscriptionCatalogQuery {
  private final SubscriptionCatalogRepository subscriptionCatalogRepository;

  public SubscriptionCatalogQueryImpl(SubscriptionCatalogRepository subscriptionCatalogRepository) {
    this.subscriptionCatalogRepository = subscriptionCatalogRepository;
  }

  @Override
  public Subscription getSubscription(Long subscriptionId) {
    var subscriptionCatalog = new SubscriptionCatalog(subscriptionCatalogRepository);
    return subscriptionCatalog.get(subscriptionId);
  }

  @Override
  public List<Subscription> getSubscriptions() {
    var subscriptionCatalog = new SubscriptionCatalog(subscriptionCatalogRepository);
    return subscriptionCatalog.getAll();
  }
}
