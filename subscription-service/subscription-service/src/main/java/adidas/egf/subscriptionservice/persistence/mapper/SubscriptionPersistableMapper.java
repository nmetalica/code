package adidas.egf.subscriptionservice.persistence.mapper;

import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.persistence.repository.model.SubscriptionPersistable;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class SubscriptionPersistableMapper {

  public static final SubscriptionPersistableMapper MAPPER =
      Mappers.getMapper(SubscriptionPersistableMapper.class);

  @Mapping(source = "subscriptionId", target = "id")
  public abstract SubscriptionPersistable map(Subscription subscription);

  @Mapping(source = "id", target = "subscriptionId")
  public abstract Subscription map(SubscriptionPersistable subscriptionPersistable);
}
