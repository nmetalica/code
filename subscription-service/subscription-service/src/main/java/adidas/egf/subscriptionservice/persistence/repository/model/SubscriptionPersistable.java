package adidas.egf.subscriptionservice.persistence.repository.model;

import adidas.egf.subscriptionservice.application.domain.model.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@ToString
@Setter
@Getter
@Entity
@Table(name = "subscription_catalog")
public class SubscriptionPersistable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String subscriptionStatus;
  private String name;
  private String email;
  private Gender gender;
  private Date birthDate;
  private boolean flag;
  private Long newsletterId;
}
