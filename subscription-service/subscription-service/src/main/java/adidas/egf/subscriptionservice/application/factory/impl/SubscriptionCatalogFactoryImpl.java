package adidas.egf.subscriptionservice.application.factory.impl;

import adidas.egf.subscriptionservice.application.query.SubscriptionCatalogQuery;
import adidas.egf.subscriptionservice.application.command.SubscriptionCatalogCommand;
import adidas.egf.subscriptionservice.application.factory.SubscriptionCatalogFactory;

public class SubscriptionCatalogFactoryImpl implements SubscriptionCatalogFactory {

  private final SubscriptionCatalogCommand subscriptionCatalogCommand;
  private final SubscriptionCatalogQuery subscriptionCatalogQuery;

  public SubscriptionCatalogFactoryImpl(
          SubscriptionCatalogCommand subscriptionCatalogCommand, SubscriptionCatalogQuery subscriptionCatalogQuery) {
    this.subscriptionCatalogCommand = subscriptionCatalogCommand;
    this.subscriptionCatalogQuery = subscriptionCatalogQuery;
  }

  @Override
  public SubscriptionCatalogCommand getSubscriptionCatalogCommand() {
    return subscriptionCatalogCommand;
  }

  @Override
  public SubscriptionCatalogQuery getSubscriptionQuery() {
    return subscriptionCatalogQuery;
  }
}
