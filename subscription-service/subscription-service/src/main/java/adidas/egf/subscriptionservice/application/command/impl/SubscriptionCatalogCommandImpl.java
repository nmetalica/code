package adidas.egf.subscriptionservice.application.command.impl;

import adidas.egf.subscriptionservice.application.command.SubscriptionCatalogCommand;
import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.application.domain.model.SubscriptionCatalog;
import adidas.egf.subscriptionservice.application.domain.repository.SubscriptionCatalogRepository;

public class SubscriptionCatalogCommandImpl implements SubscriptionCatalogCommand {
  private final SubscriptionCatalogRepository subscriptionCatalogRepository;

  public SubscriptionCatalogCommandImpl(SubscriptionCatalogRepository subscriptionCatalogRepository) {
    this.subscriptionCatalogRepository = subscriptionCatalogRepository;
  }

  @Override
  public Long addSubscription(Subscription subscription) {
    var subscriptionCatalog = new SubscriptionCatalog(subscriptionCatalogRepository);
    return subscriptionCatalog.addSubscription(subscription);
  }

  @Override
  public Long cancelSubscription(Subscription subscription) {
    var subscriptionCatalog = new SubscriptionCatalog(subscriptionCatalogRepository);
    return subscriptionCatalog.cancelSubscription(subscription);
  }
}
