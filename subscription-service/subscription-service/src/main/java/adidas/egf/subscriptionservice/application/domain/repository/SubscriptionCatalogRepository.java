package adidas.egf.subscriptionservice.application.domain.repository;

import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.application.query.model.QueryParameter;

import java.util.List;

/** Domain repository for the subscription catalog. */
public interface SubscriptionCatalogRepository {
  Long save(Subscription subscription);

  Subscription get(Long subscriptionId);

  List<Subscription> getAll();

  void delete(Long subscriptionId);
}
