package adidas.egf.subscriptionservice.application.factory;

import adidas.egf.subscriptionservice.application.query.SubscriptionCatalogQuery;
import adidas.egf.subscriptionservice.application.command.SubscriptionCatalogCommand;

public interface SubscriptionCatalogFactory {

  SubscriptionCatalogCommand getSubscriptionCatalogCommand();

  SubscriptionCatalogQuery getSubscriptionQuery();
}
