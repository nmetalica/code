package adidas.egf.subscriptionservice.persistence.repository;

import adidas.egf.subscriptionservice.application.domain.model.Subscription;
import adidas.egf.subscriptionservice.application.domain.repository.SubscriptionCatalogRepository;
import adidas.egf.subscriptionservice.application.query.model.QueryParameter;
import adidas.egf.subscriptionservice.common.error.ResourceNotFoundException;
import adidas.egf.subscriptionservice.persistence.mapper.SubscriptionPersistableMapper;
import adidas.egf.subscriptionservice.persistence.repository.jpa.SubscriptionCatalogJpaRepository;
import adidas.egf.subscriptionservice.persistence.repository.model.SubscriptionPersistable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SubscriptionCatalogRepositoryImpl implements SubscriptionCatalogRepository {

  private final SubscriptionCatalogJpaRepository subscriptionCatalogJpaRepository;

  public SubscriptionCatalogRepositoryImpl(SubscriptionCatalogJpaRepository subscriptionCatalogJpaRepository) {
    this.subscriptionCatalogJpaRepository = subscriptionCatalogJpaRepository;
  }

  @Override
  public Long save(Subscription subscription) {
    //TODO check if there is a subscription email+newsletterId ?
    var entity = SubscriptionPersistableMapper.MAPPER.map(subscription);
    entity = subscriptionCatalogJpaRepository.save(entity);
    return entity.getId();
  }

  @Override
  public Subscription get(Long subscriptionId) {
    Optional<SubscriptionPersistable> subscriptionPersistable =
            subscriptionCatalogJpaRepository.findById(subscriptionId);
    return subscriptionPersistable
        .map(SubscriptionPersistableMapper.MAPPER::map)
        .orElseThrow(() -> new ResourceNotFoundException("Subscription not found"));
  }

  @Override
  public List<Subscription> getAll() {
    var subscriptionPersistables = subscriptionCatalogJpaRepository.findAll();
    return subscriptionPersistables.stream()
        .map(SubscriptionPersistableMapper.MAPPER::map)
        .collect(Collectors.toList());
  }

  @Override
  public void delete(Long subscriptionId) {
    subscriptionCatalogJpaRepository.deleteById(subscriptionId);
  }
}
