package adidas.egf.subscriptionservice.resource.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Setter
@Getter
@ToString
public class SubscriptionRequest {
  private Long subscriptionId;

  @NotNull(message = "Name is required.")
  @NotBlank(message = "Name cannot be blank.")
  private String name;

  @NotNull(message = "Gender is required.")
  @NotBlank(message = "Gender cannot be blank.")
  private String gender;

  private String email;

  private Date birthDate;

  private boolean flag;

  private Long newsletterId;
}
