package adidas.egf.publicservice.resource.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import adidas.egf.publicservice.model.Subscription;
import adidas.egf.publicservice.model.GetSubscriptionResponse;
import adidas.egf.publicservice.model.SubscriptionRequest;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class SubscriptionRequestMapper {

  public static final SubscriptionRequestMapper MAPPER = Mappers.getMapper(SubscriptionRequestMapper.class);

  public abstract Subscription map(SubscriptionRequest request);

  public abstract GetSubscriptionResponse map(Subscription product);
}
