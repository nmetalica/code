package adidas.egf.publicservice.model;

import lombok.*;

import java.util.Date;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
public class Subscription {
  private Long subscriptionId;
  private String subscriptionStatus;
  private String name;
  private String email;
  private Gender gender;
  private Date birthDate;
  private boolean flag;
  private Long newsletterId;
}
