package adidas.egf.publicservice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import adidas.egf.publicservice.model.Gender;

import java.util.Date;

@Setter
@Getter
@ToString
public class GetSubscriptionResponse {
  private Long subscriptionId;
  private String subscriptionStatus;
  private String name;
  private String email;
  private Gender gender;
  private Date birthDate;
  private boolean flag;
  private Long newsletterId;
}
