package adidas.egf.publicservice.resource;

import adidas.egf.publicservice.model.GetSubscriptionResponse;
import adidas.egf.publicservice.model.SubscriptionRequest;
import adidas.egf.publicservice.model.SubscriptionResponse;
import adidas.egf.publicservice.resource.mapper.SubscriptionRequestMapper;
import adidas.egf.publicservice.service.SubscriptionService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class PublicServiceController {


  @Autowired
  SubscriptionService subscriptionService;


  @PostMapping("/subscription")
  public ResponseEntity<SubscriptionResponse> createSubscription(
       @RequestBody SubscriptionRequest subscriptionRequest) {

    log.info("Creating subscription, name {}", subscriptionRequest.getName());
    var subscription = SubscriptionRequestMapper.MAPPER.map(subscriptionRequest);
    var subscriptionResponse = subscriptionService.addSubscription(subscription);
    return new ResponseEntity<>(subscriptionResponse, HttpStatus.CREATED);
  }

  @GetMapping("/subscription/{subscriptionId}")
  public ResponseEntity<GetSubscriptionResponse> getSubscriptionById(
       @PathVariable @NonNull Long subscriptionId) {

    log.info("Getting subscription, id {}", subscriptionId);
    var subscription = subscriptionService.getSubscription(subscriptionId);
    return new ResponseEntity<>(subscription, HttpStatus.OK);
  }

  @GetMapping("/subscriptions")
  public ResponseEntity<List<GetSubscriptionResponse>> listSubscriptions() {

    log.info("Getting all subscriptions");
    var subscriptions = subscriptionService.getSubscriptions();
    return new ResponseEntity<>(subscriptions, HttpStatus.OK);
  }

  @PutMapping("/subscription/{subscriptionId}")
  public ResponseEntity<Void> cancelSubscription(
           @PathVariable @NonNull Long subscriptionId) {

    log.info("Deleting subscription, id {}", subscriptionId);
    subscriptionService.cancelSubscription(subscriptionId);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
