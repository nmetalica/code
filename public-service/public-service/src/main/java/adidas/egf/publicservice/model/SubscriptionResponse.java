package adidas.egf.publicservice.model;

import lombok.*;

@Setter
@Getter
@ToString
public class SubscriptionResponse {
  private Long subscriptionId;
}
