package adidas.egf.publicservice.service;

import adidas.egf.publicservice.common.config.FeignClientConfiguration;
import adidas.egf.publicservice.model.GetSubscriptionResponse;
import adidas.egf.publicservice.model.Subscription;
import adidas.egf.publicservice.model.SubscriptionResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@FeignClient(name = "subscription", url = "localhost:8080", path = "/subscriptionService", configuration = FeignClientConfiguration.class)
public interface SubscriptionService {

    @PostMapping(value = "/subscription")
    SubscriptionResponse addSubscription(@RequestBody Subscription subscription);

    @PutMapping(value = "/subscription/{subscriptionId}")
    Long  cancelSubscription(@PathVariable Long subscriptionId);

    @GetMapping(value = "/subscription/{subscriptionId}")
    GetSubscriptionResponse getSubscription(@PathVariable Long subscriptionId);

    @GetMapping(value = "/subscriptions" )
    List<GetSubscriptionResponse> getSubscriptions();
}
