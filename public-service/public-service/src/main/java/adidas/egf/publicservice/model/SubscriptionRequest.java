package adidas.egf.publicservice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.util.Date;

@Setter
@Getter
@ToString
public class SubscriptionRequest {
  private Long subscriptionId;


  private String name;


  private String gender;

  private String email;

  private Date birthDate;

  private boolean flag;

  private Long newsletterId;
}
