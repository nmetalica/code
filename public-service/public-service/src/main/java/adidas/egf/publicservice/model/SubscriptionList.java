package adidas.egf.publicservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class SubscriptionList {
    private List<GetSubscriptionResponse> subscriptions;

    public SubscriptionList(){
        subscriptions = new ArrayList<>();
    }
}
