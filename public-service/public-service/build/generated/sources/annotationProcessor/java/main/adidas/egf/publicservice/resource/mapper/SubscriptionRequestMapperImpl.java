package adidas.egf.publicservice.resource.mapper;

import adidas.egf.publicservice.model.Gender;
import adidas.egf.publicservice.model.GetSubscriptionResponse;
import adidas.egf.publicservice.model.Subscription;
import adidas.egf.publicservice.model.Subscription.SubscriptionBuilder;
import adidas.egf.publicservice.model.SubscriptionRequest;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-01-28T14:05:23+0100",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.1.1.jar, environment: Java 11 (Oracle Corporation)"
)
public class SubscriptionRequestMapperImpl extends SubscriptionRequestMapper {

    @Override
    public Subscription map(SubscriptionRequest request) {
        if ( request == null ) {
            return null;
        }

        SubscriptionBuilder subscription = Subscription.builder();

        subscription.subscriptionId( request.getSubscriptionId() );
        subscription.name( request.getName() );
        subscription.email( request.getEmail() );
        if ( request.getGender() != null ) {
            subscription.gender( Enum.valueOf( Gender.class, request.getGender() ) );
        }
        subscription.birthDate( request.getBirthDate() );
        subscription.flag( request.isFlag() );
        subscription.newsletterId( request.getNewsletterId() );

        return subscription.build();
    }

    @Override
    public GetSubscriptionResponse map(Subscription product) {
        if ( product == null ) {
            return null;
        }

        GetSubscriptionResponse getSubscriptionResponse = new GetSubscriptionResponse();

        getSubscriptionResponse.setSubscriptionId( product.getSubscriptionId() );
        getSubscriptionResponse.setSubscriptionStatus( product.getSubscriptionStatus() );
        getSubscriptionResponse.setName( product.getName() );
        getSubscriptionResponse.setEmail( product.getEmail() );
        getSubscriptionResponse.setGender( product.getGender() );
        getSubscriptionResponse.setBirthDate( product.getBirthDate() );
        getSubscriptionResponse.setFlag( product.isFlag() );
        getSubscriptionResponse.setNewsletterId( product.getNewsletterId() );

        return getSubscriptionResponse;
    }
}
